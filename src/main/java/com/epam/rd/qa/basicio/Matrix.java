package com.epam.rd.qa.basicio;

import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.ArgumentMatchers.intThat;

/**
 * Encapsulates two-dimensional array-matrix block of real ({@code double}) type.
 * {@code Matrix} is the cover for two-dimensional array of real values, storing matrix
 * values with operations of matrix addition, deduction, and multiplication.
 */
public class Matrix {
    private double[][] values;

    /**
     * Creates an empty matrix with predetermined number
     * of rows and columns (all values in matrix equal to 0)
     *
     * @param rows number of rows
     * @param cols number of columns
     * @throws MatrixException if {@code rows} or {@code cols} less than 1
     */
    public Matrix(int rows, int cols) throws MatrixException {
        if (rows < 1 && cols < 1) {
            throw new MatrixException();
        }else {
            values = new double[rows][cols];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    values[i][j]=0;
                }
            }
        }
    }

    /**
     * Creates a matrix based on existing two-dimensional array
     *
     * @param values two-dimensional array
     * @throws MatrixException if {@code rows} or {@code cols} less than 1
     */
    public Matrix(double[][] values) throws MatrixException {
        if (values.length < 1 || values[0].length < 1) {
            throw new MatrixException();
        }
        this.values = values;
    }

    /**
     * Returns count of matrix rows.
     *
     * @return count of rows in the matrix
     */
    public int getRows() {
        return values.length;
    }

    /**
     * Returns count of matrix columns
     *
     * @return count of columns in the matrix
     */
    public int getColumns() {
        return values[0].length;
    }

    /**
     * Returns an element via predetermined correct indexes.
     *
     * @param row row index
     * @param col column index
     * @return the element via indexes
     * @throws MatrixException if index out of bounds
     */
    public double get(int row, int col) throws MatrixException{
        if (row < 0 || col < 0 || getRows() <= row || getColumns() <= col) {
            throw new MatrixException();
        }else
            return values[row][col];
    }

    /**
     * Sets new value via predetermined correct indexes.
     *
     * @param row   row index
     * @param col   column index
     * @param value value to set
     * @throws MatrixException if index out of bounds
     */
    public void set(int row, int col, double value) throws MatrixException {
        if (row < 0 || col < 0 || getRows() <= row || getColumns() <= col) {
            throw new MatrixException();
        }else {
            values[row][col]=value;
        }

    }

    /**
     * Returns standard two-dimensional array out of matrix.
     * Any changes in the returned array will be reflected to internal array.
     *
     * @return matrix values
     */
    public double[][] toArray() {
        return values;
    }

    /**
     * Adds all elements of {@code other} matrix to corresponding elements
     * of this matrix and creates new {@code Matrix} with resulting two-dimensional array
     *
     * @param other another {@code Matrix} object
     * @return new matrix
     * @throws MatrixException if matrices have different size
     */
    public Matrix add(Matrix other) throws MatrixException {
        double otherMatrix[][] = other.toArray();
        if (getRows()!=other.getRows() || getColumns() != other.getColumns()) {
            throw new MatrixException();
        }else {
            for (int i = 0; i < otherMatrix.length;i++) {
                for (int j = 0; j < otherMatrix[i].length; j++) {
                    otherMatrix[i][j]+=values[i][j];
                }
            }
        }
        return other;
    }

    /**
     * Subtract all elements of {@code other} matrix from corresponding elements
     * of this matrix and creates new {@code Matrix} with resulting two-dimensional array
     *
     * @param other another {@code Matrix} object
     * @return new matrix
     * @throws MatrixException if matrices have different size
     */
    public Matrix subtract(Matrix other) throws MatrixException {
        double otherMatrix[][] = other.toArray();
        if (getRows()!=other.getRows() || getColumns() != other.getColumns()) {
            throw new MatrixException();
        }else {
            for (int i = 0; i < otherMatrix.length;i++) {
                for (int j = 0; j < otherMatrix[i].length; j++) {
                    otherMatrix[i][j]=values[i][j] - otherMatrix[i][j];
                }
            }
        }
        return other;
    }

    /**
     * Multiply this matrix to {@code other} matrix.<br/>
     * See
     * <a href="https://en.wikipedia.org/wiki/Matrix_multiplication">Matrix multiplication</a>
     * <a href="https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm">Matrix multiplication algorithm</a>
     *
     * @param other another matrix
     * @return new matrix
     * @throws MatrixException if matrices have non-compliant sizes
     */
    public Matrix multiply(Matrix other) throws MatrixException {

        double otherMatrix[][] = other.toArray();
        int m = values.length;
        int n = otherMatrix[0].length;
        int o = otherMatrix.length;
        double[][] res = new double[m][n];
        if (getRows() != other.getColumns() || getColumns() != other.getRows()) {
            throw new MatrixException();
        }else {
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    for (int k = 0; k < o; k++) {
                        res[i][j] += values[i][k] * otherMatrix[k][j];
                    }
                }
            }
        }
        Matrix resultMatrix = new Matrix(res);
        return resultMatrix;
    }
}