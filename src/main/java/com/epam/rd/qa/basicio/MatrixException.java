package com.epam.rd.qa.basicio;

public class MatrixException extends RuntimeException{
    @Override
    public String getMessage() {
        return "Unacceptable value!";
    }
}